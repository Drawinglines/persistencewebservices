package com.terra_ai.filters;

import java.util.ArrayList;
import java.util.Collection;

import com.terra_ai.model.business.Group;

import io.almostrealism.persist.ValueFilter;

public class GroupNameFilter extends ValueFilter<Group> {
	private String name;
	
	public GroupNameFilter(String name) { this.name = name; }
	
	@Override
	public Collection<? extends Group> filter(Collection<? extends Group> values) throws CloneNotSupportedException {
		Collection<Group> fields = new ArrayList<Group>();
		for (Group g : values) if (g.getName().equals(name)) fields.add(g);
		return fields;
	}
}
