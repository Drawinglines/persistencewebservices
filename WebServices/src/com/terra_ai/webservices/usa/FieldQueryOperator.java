package com.terra_ai.webservices.usa;

import java.util.Collection;

import com.terra_ai.filters.CountyNameFilter;
import com.terra_ai.filters.GroupNameFilter;
import com.terra_ai.filters.FilterUtilities;
import com.terra_ai.filters.WellTypeFilter;
import com.terra_ai.model.business.Group;

public class FieldQueryOperator {
	private FieldQuery query;
	
	public FieldQueryOperator(FieldQuery q) { this.query = q; }
	
	public Collection<? extends Group> applyFilters(Collection<? extends Group> f) throws CloneNotSupportedException {
		String county = FilterUtilities.preprocessFilterValue(query.getCounty());
		
		if (county != null && county.length() > 0)
			f = new CountyNameFilter(county).filter(f);
		
		String field = FilterUtilities.preprocessFilterValue(query.getField());
		
		if (field != null && field.length() > 0)
			f = new GroupNameFilter(field).filter(f);
		
		String wellType = FilterUtilities.preprocessFilterValue(query.getWellType());
		
		if (wellType != null && wellType.length() > 0)
			FilterUtilities.filterGroups(f, new WellTypeFilter(wellType));
		
//		TODO  Which value should we use here?
//		if (query.getStatusCalculated() != null && query.getStatusCalculated().length() > 0) 
//			FilterUtilities.filterGroups(f, new WellStatusFilter(query.getStatusCalculated()));
		
		return f;
	}
}
