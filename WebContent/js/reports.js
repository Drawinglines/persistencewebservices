//var persistenceService = "http://terra.us-west-2.elasticbeanstalk.com/";
//var persistenceService = "http://localhost:8080/PersistenceWebServices/";
var persistenceService = "";
var supersetUrl = "http://superset.terra.ai:8088"

/**
 * Draw the line chart.
 */
function linechart(api) {
	$.post(persistenceService + "rest/usa/wells/", { apis : getSelectedAPIs(), token : getCookie("token") },
			function(object) {

		var margin = {top: 30, right: 180, bottom: 100, left: 60},
		margin2 = {top: 580, right: 180, bottom: 20, left: 60},
		width = 960 - margin.left - margin.right,
		height = 650 - margin.top - margin.bottom,
		height2 = 650 - margin2.top - margin2.bottom;

		var parseDate = d3.time.format("%Y-%m-%d").parse; //to parse the date in proper format

		var xScale = d3.time.scale().range([0, width]),  //main graph
		yScale = d3.scale.linear().range([height, 0]), //main graph
		x2Scale = d3.time.scale().range([0, width]),  //mini graph - brushing  this is not required could have used xScale itself its the same thing
		y2Scale = d3.scale.linear().range([height2, 0]), // mini graph - brushing
		y3Scale = d3.scale.linear().range([height,0]), //to plot wor on the main graph
		y4Scale = d3.scale.linear().range([height2,0]), // to plot wor on the bottom graph

		xAxis = d3.svg.axis().scale(xScale).orient("bottom"),
		xAxis2 = d3.svg.axis().scale(x2Scale).orient("bottom"),

//		Left Y axis for water, oil		
		yAxis = d3.svg.axis().scale(yScale).orient("left"),

//		Right Y axis for wor		
		yAxis3 = d3.svg.axis().scale(y3Scale).orient("right");

		var brush = d3.svg.brush().x(x2Scale).on("brush", brush);

		var line = d3.svg.line() // top graph line function
		.defined(function(d) { return !isNaN(d.welldata); })
		.interpolate("linear")
		.x(function(d) { return xScale(d.date); })
		.y(function(d) { return yScale(d.welldata); });

		var line2 = d3.svg.line()  // bottom graph line function
		.defined(function(d) { return !isNaN(d.welldata); })
		.interpolate("linear")
		.x(function(d) {return x2Scale(d.date); })
		.y(function(d) {return y2Scale(d.welldata); });	

		var line3 = d3.svg.line() //wor line function main graph
		.defined(function(d) { return !isNaN(d.wor);})
		.interpolate("linear")
		.x(function(d) {return xScale(d.date);})
		.y(function(d) {return y3Scale(d.wor);});

		var line4 = d3.svg.line() // wor line function bottom graph
		.defined(function(d) {return !isNaN(d.wor);})
		.interpolate("linear")
		.x(function(d) {return x2Scale(d.date); })
		.y(function(d) {return y4Scale(d.wor);});
		
		var line5 = d3.svg.line() // top forecast graph line function
		.defined(function(d) { return !isNaN(d.amount); })
		.interpolate("linear")
		.x(function(d) { return xScale(d.date); })
		.y(function(d) { return yScale(d.amount); });

		var line6 = d3.svg.line()  // bottom forecast graph line function
		.defined(function(d) { return !isNaN(d.amount); })
		.interpolate("linear")
		.x(function(d) {return x2Scale(d.date); })
		.y(function(d) {return y2Scale(d.amount); });	

		var svg = d3.select("#Linechart") // append the svg
		.append("svg").attr("id","line")
		.attr("width", width + margin.left + margin.right)
		.attr("height", height + margin.top + margin.bottom);

		svg.append("defs").append("clipPath")
		.attr("id", "clip")
		.append("rect")
		.attr("width", width)
		.attr("height", height);

		var focus = svg.append("g").attr("id","focus")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		var context = svg.append("g").attr("id","context")
		.attr("transform", "translate(" + margin2.left + "," + margin2.top + ")");

		//load the data to be displayed

		var data=[]
		var wor=[]
		var forecast=[]
		
		if (object.reports != null) {
			object.reports.forEach(function(entry) {
				if (entry.oil == 0){
					a = 0;
				} else {
					a = entry.water / (entry.oil)
				};
	
				data.push({"date":entry.report_date,"oil_mo":entry.oil,"water_mo":entry.water  })
				wor.push({"date":entry.report_date,"wor":entry.wor})
			});
		}
		
		// TODO  Load forecasts dynamically for whichever forecasts are
		//       available by name rather than naming them directly
		if (object.oilForecasts.test_utah_app != null) {
			object.oilForecasts.test_utah_app.forEach(function(entry) {
				forecast.push({"date":entry.date, "amount":entry.amountPerMonth});
			});
		} else if (object.oilForecasts.test_NDK_nn != null) {
			object.oilForecasts.test_NDK_nn.forEach(function(entry) {
				forecast.push({"date":entry.date, "amount":entry.amountPerMonth});
			});
		}
		
		data.forEach(function(d) { d.date = parseDate(d.date); });
		wor.forEach(function(d) { d.date = parseDate(d.date); });
		forecast.forEach(function(d) { d.date = parseDate(d.date); });

		data = data.sort(sortByDateAsc)
		wor = wor.sort(sortByDateAsc)
		forecast = forecast.sort(sortByDateAsc);

		function sortByDateAsc(a,b) { return a.date - b.date; }

		var sources = d3.keys(data[0]).filter(function(key) { return key !== "date"; })
		.map(function(name) {
			return {
				name: name,
				values: data.map(function(d) {
					return {date: d.date, welldata: +d[name]};
				})
			};
		});

		xScale.domain(d3.extent(data, function(d) { return d.date; }));
		yScale.domain([d3.min(sources, function(c) { return d3.min(c.values, function(v) { return v.welldata; }); }),
		               d3.max(sources, function(c) { return d3.max(c.values, function(v) { return v.welldata; }); }) ]);
		x2Scale.domain(xScale.domain());
		y2Scale.domain(yScale.domain());

		y3Scale.domain([d3.min(wor, function(c) { return Math.min(c.wor); } ),
		                d3.max(wor,function(c) { return Math.max(c.wor); })]);
		y4Scale.domain(y3Scale.domain());

		var focuslineGroups = focus.selectAll("g")
		.data(sources)
		.enter().append("g");

		//Draw the main lines
		var focuslines = focuslineGroups.append("path")
		.attr("class","line dataline")
		.attr("d", function(d) { return line(d.values); })
		.attr("class",function(d) {return d.name+" dataline" ;})
		.attr("clip-path", "url(#clip)");

		//Add the wor line
		var focuslineGroups2=focus
		.append("g")
		.attr("id","WOR");

		var focuslines2 = focuslineGroups2.append("path")
		.datum(wor)
		.attr("class","wor dataline")
		.attr("d",function(d) {return line3(d);})
		.attr("clip-path", "url(#clip)");
		
		// Add the forecast line
		var focuslineGroups3=focus
		.append("g")
		.attr("id","forecast");
		
		var focuslines3 = focuslineGroups3.append("path")
		.datum(forecast)
		.attr("class","forecast dataline")
		.attr("d",function(d) {return line5(d);})
		.attr("clip-path", "url(#clip)");


		// Draw the x Grid lines
		focus.append("g")
		.attr("class", "grid")
		.attr("id","xGrid")
		.attr("transform", "translate(0," + height + ")")
		.call(make_x_axis()
				.tickSize(-height, 0, 0)
				.tickFormat("")
		);

		// Draw the y Grid lines
		focus.append("g")            
		.attr("class", "grid")
		.attr("id","yGrid")
		.call(make_y_axis()
				.tickSize(-width, 0, 0)
				.tickFormat("")
		);

		//Add a line on top to bound the graph
		focus.append("g")
		.append("line")
		.attr("class","line")
		.attr("x1",0)
		.attr("y1",0)
		.attr("x2", width)
		.attr("y2",0);

		// Add the X Axis
		focus.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height + ")")
		.call(xAxis)
		.append("text")
		.text("Report Date")
		.attr("y",30)
		.attr("x",300)
		.attr("font-family", "sans-serif")
		.style("font-size", 14);


		// Add the Y Axis	
		focus.append("g")
		.attr("class", "y axis")
		//.style("fill", "steelblue")
		.call(yAxis)
		.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y",-48)
		.attr("x",-160)
		.style("text-anchor", "end")
		.style("font-size", 14)
		.text("Monthly Flow (barrels)");

		// Add the second Y axis for WOR
		focus.append("g")
		.attr("class","y axis")
		.attr("transform", "translate(" + width + " ,0)")   
		//.style("fill", "red")   
		.call(yAxis3).append("text")
		.attr("transform", "rotate(-90)")
		.attr("y",40)
		.attr("x",-220)
		.style("text-anchor", "end")
		.style("font-size", 14)
		.text("WOR");

		// Add the bottom graph
		var contextlineGroups = context.selectAll("g")
		.data(sources)
		.enter().append("g");

		var contextLines = contextlineGroups.append("path")
		.attr("class", "line")
		.attr("d", function(d) { return line2(d.values); })
		//.style("stroke", function(d) {return (d.name);})
		.attr("class",function(d) {return d.name;})
		.attr("clip-path", "url(#clip)");

		context.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + height2 + ")")
		.call(xAxis2);

		context.append("g")
		.attr("class", "x brush")
		.call(brush)
		.selectAll("rect")
		.attr("y", -6)
		.attr("height", height2 + 7);

		//Add the wor line to bottom graph
		var contextlineGroups2=context.append("g");

		var contextlines2 = contextlineGroups2.append("path").datum(wor)
		//.attr("class","line3")
		.attr("d",function(d) {return line4(d);})
		.attr("class","wor")
		.attr("clip-path", "url(#clip)");


		function brush() {
			xScale.domain(brush.empty() ? x2Scale.domain() : brush.extent());
			focus.selectAll("path.oil_mo").attr("d",  function(d) {return line(d.values)});
			focus.selectAll("path.water_mo").attr("d",  function(d) {return line(d.values)});
			focus.selectAll("path.wor").attr("d",  function(d) {return line3(d)});
			focus.select(".x.axis").call(xAxis);
			focus.select(".y.axis").call(yAxis);
			focus.select("g#xGrid").call(make_x_axis()
					.tickSize(-height, 0, 0)
					.tickFormat(""));
		}

//		function for the x grid lines
		function make_x_axis() {
			return d3.svg.axis()
			.scale(xScale)
			.orient("bottom");

		}

//		function for the y grid lines
		function make_y_axis() {
			return d3.svg.axis()
			.scale(yScale)
			.orient("left");

		}

//		Add the legend for the lines and keep space for values
		var legend_colors=[{name:"Oil",color:"green"},{name:"Water",color:"DodgerBlue"},{name:"WOR",color:"red"}]

//		add g to contain current mouse point numbers
		var legendLabelGroup = svg.append("svg:g")
		.attr("class", "legend-group")
		.selectAll("g")
		.data(legend_colors)
		.enter().append("g")
		.attr("class", "legend-labels");

//		Add text near values		
		/*legendLabelGroup.append("svg:text")
.attr("class", "legend name")
.text(function(d, i) {
	return d.name;
})
.attr("font-size", 16)
.attr("fill", function(d, i) {
	// return the color for this row
	return d.color;
})
.attr("x", function(d,i) {return 225+(i*150)})
.attr("y", 120);*/

//		put in placeholders with 0 width that we'll populate and resize dynamically
		legendLabelGroup.append("svg:text")
		.attr("class", "legend value")
		.attr("id",function(d,i){return "cvals_"+i})
		.attr("font-size", 15)
		.attr("fill", function(d, i) {return d.color;})
		.attr("x", function(d,i) {return 220+(i*150)})
		.attr("y", 105);

//		Add the legend boxes and text				
		legendLabelGroup.selectAll(".legend_box")
		.data(legend_colors)
		.enter()
		.append("rect")
		.attr("class","legend_box")
		.attr("y", 5)
		.attr("x", function(d,i) {return 220+(i*150)})
		.attr("fill",function(d) {return d.color})
		.attr("width", 15)
		.attr("height", 15)
		.attr("opacity",0.8);

		legendLabelGroup.selectAll(".legend_text")
		.data(legend_colors)
		.enter()
		.append("text")
		.attr("y", 19)
		.attr("x", function(d,i) {return 245+(i*150)})
		.attr("font-family", "sans-serif")
		.attr("font-size", "16px")
		.text(function(d) {return d.name});				

//		Add linechart tooltip the commented code puts the values on the line chart near the points

		var mouseG = focus.append("g")
		.attr("class","mouse-over-effects");

//		this is the vertical line
		mouseG.append("path")
		.attr("class", "mouse-line")
		.style("stroke", "black")
		.style("stroke-width", "1px")
		.style("opacity", "1");

//		keep a reference to all our lines
		var lines = document.getElementsByClassName('dataline');


//		here's a g for each circle and text on the line
		var mousePerLine = mouseG.selectAll('.mouse-per-line')
		.data(data)
		.enter()
		.append("g")
		.attr("class", "mouse-per-line");

		// the circle
		/*mousePerLine.append("circle")
  .attr("r", 7)
  .style("stroke","orange")
  .style("fill", "black")
  .style("stroke-width", "0.5px")
  .style("opacity", "1");*/

//		the text
		mousePerLine.append("text")
		.attr("transform", "translate(10,3)")
		.attr("font-size","13px");

		var formatTime = d3.time.format("%Y-%m-%d");

//		rect to capture mouse movements
		mouseG.append('svg:rect')
		.attr('width', width)
		.attr('height', height)
		.attr('fill', 'none')
		.attr('pointer-events', 'all')
		.on('mouseout', function() { // on mouse out hide line, circles and text
			d3.select(".mouse-line")
			.style("opacity", "0");
			//d3.selectAll(".mouse-per-line circle")
			// .style("opacity", "0");
			d3.selectAll(".mouse-per-line text")
			.style("opacity", "0");
			svg.selectAll("text.legend.value").text("")
		})
		.on('mouseover', function() { // on mouse in show line, circles and text
			d3.select(".mouse-line")
			.style("opacity", "1");
			//d3.selectAll(".mouse-per-line circle")
			//  .style("opacity", "1");
			d3.selectAll(".mouse-per-line text")
			.style("opacity", "1");
		})
		.on('mousemove', function() { // mouse moving over canvas
			var mouse = d3.mouse(this);

			// move the vertical line
			d3.select(".mouse-line")
			.attr("d", function() {
				var d = "M" + mouse[0] + "," + height;
				d += " " + mouse[0] + "," + 0;
				return d;
			});

			// position the circle and text
			d3.selectAll(".mouse-per-line")
			.style("opacity", "1")
			.attr("transform", function(d, i) {
				//console.log(width/mouse[0])

				var xDate = xScale.invert(mouse[0]),
				bisect = d3.bisector(function(d) { return d.date; }).right;

				//idx = bisect(d.oil_mo, xDate);
				//idx2 = bisect(d.water_mo,xDate)

				// this conducts a search using some SVG path functions
				// to find the correct position on the line
				// from http://bl.ocks.org/duopixel/3824661

				try{
					var beginning = 0,
					end = lines[i].getTotalLength(),
					target = null;
				} catch(err) { }

				w: while (true) {
					target = Math.floor((beginning + end) / 2);
					
					if (lines[i] == null) break w;
					pos = lines[i].getPointAtLength(target);

					if ((target === end || target === beginning) && pos.x !== mouse[0]) {
						break;
					}
					if (pos.x > mouse[0])      end = target;
					else if (pos.x < mouse[0]) beginning = target;
					else break; //position found

				}

				// update the text with y value
				if (i!=2) {
					//d3.select(this).select('text')
					// .text(yScale.invert(pos.y).toFixed(2));

					svg.selectAll("text#cvals_"+i+".legend.value").text(yScale.invert(pos.y).toFixed(2))
				} else {
					//d3.select(this).select('text')
					// .text(y3Scale.invert(pos.y).toFixed(2));

					svg.selectAll("text#cvals_"+i+".legend.value").text(y3Scale.invert(pos.y).toFixed(2))

					//Add the date on top. This is not the best way to do it but it works so....
					d3.select(this).select('text').text(formatTime(xDate))
				}

				// return position
				return "translate(" + mouse[0] + ",-5)";
			});
		});
	});
}

function populateOperators(apis) {
	document.getElementById("operators_iframe").src = supersetUrl + "/superset/explore/table/10/?add_to_dash=false&viz_type=pie&" +
														"metrics=sum__cum_oil&force=true&flt_eq_0=&new_slice_name=&until=now&" +
														"groupby=operator_name&where=api IN (" + apis + ") &" +
														"since=7+days+ago&new_dashboard_name=&previous_viz_type=pie&flt_col_0=&" +
														"granularity_sqla=None&limit=50&datasource_id=10&datasource_type=table&" +
														"datasource_name=superset.well_head_by_operator&userid=1&rdo_save=overwrite&" +
														"save_to_dashboard_id=&show_legend=y&show_legend=false&standalone=true&" +
														"goto_dash=false&time_grain_sqla=Time+Column&flt_op_0=in&having=&labels_outside=y&" +
														"labels_outside=false&slice_name=Operator+Breakdown&donut=y&donut=false&" +
														"pie_label_type=key&collapsed_fieldsets=&height=600";
}

function populatePools(apis) {
	document.getElementById("pools_iframe").src = supersetUrl + "/superset/explore/table/11/?add_to_dash=false&viz_type=pie&" +
														"metrics=sum__cum_oil&force=false&flt_eq_0=&new_slice_name=&until=now&" +
														"groupby=pool_name&where=api IN (" + apis + ") &since=7+days+ago&" +
														"new_dashboard_name=&previous_viz_type=pie&flt_col_0=&granularity_sqla=None&" +
														"limit=50&datasource_id=11&datasource_type=table&" +
														"datasource_name=superset.well_head_by_pool&userid=1&rdo_save=overwrite&" +
														"save_to_dashboard_id=&show_legend=y&show_legend=false&standalone=true&" +
														"goto_dash=false&time_grain_sqla=Time+Column&flt_op_0=in&having=&labels_outside=y&" +
														"labels_outside=false&slice_name=Pool+Breakdown&donut=y&donut=false&pie_label_type=key&" +
														"collapsed_fieldsets=&height=600";
}

function populateWellList(apis) {
	$("#well_list").empty();

	$.post(persistenceService + "rest/usa/list/", { apis : getSelectedAPIs(), token : getCookie("token") },
		function(object) {
			$("#well_list").append(object);
		}
	);
}