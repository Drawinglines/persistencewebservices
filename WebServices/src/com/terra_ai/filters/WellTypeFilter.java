package com.terra_ai.filters;

import java.util.ArrayList;
import java.util.Collection;

import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.ValueFilter;

public class WellTypeFilter extends ValueFilter<WellHead> {
	private String type;
	
	public WellTypeFilter(String wellType) { this.type = wellType; }
	
	@Override
	public Collection<WellHead> filter(Collection<? extends WellHead> values) throws CloneNotSupportedException {
		Collection<WellHead> wells = new ArrayList<WellHead>();
		for (WellHead h : values) if (type.equals(h.getWellType())) wells.add(h);
		return wells;
	}
}
