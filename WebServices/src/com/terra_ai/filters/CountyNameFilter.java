package com.terra_ai.filters;

import java.util.Collection;
import java.util.Iterator;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.ValueFilter;

public class CountyNameFilter extends ValueFilter<Group> {
	private String name;
	
	public CountyNameFilter(String name) { this.name = name; }
	
	@Override
	public Collection<? extends Group> filter(Collection<? extends Group> values) throws CloneNotSupportedException {
		for (Group g : values) {
			Iterator<WellHead> h = g.iterator();
			
			while (h.hasNext()) {
				if (!name.equals(h.next().getCounty())) {
					h.remove();
				}
			}
		}
		
		return values;
	}
	
}
