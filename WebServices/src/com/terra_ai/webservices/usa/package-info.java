
/**
 * Web Service endpoints for querying regions of the USA.
 * 
 * @author  Michael Murray
 */
package com.terra_ai.webservices.usa;