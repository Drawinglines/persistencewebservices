package com.terra_ai.filters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.wells.WellBore;
import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.ValueFilter;

/**
 * The {@link GroupGeoFilter} filters out {@link WellBore} level data.
 * 
 * @author  Michael Murray
 */
public class GroupGeoFilter extends ValueFilter<Group> {
	@Override
	public Collection<Group> filter(Collection<? extends Group> values) throws CloneNotSupportedException {
		List<Group> fields = new ArrayList<Group>();
		
		for (Group f : values) {
			f = (Group) f.clone();
			
			List<WellHead> wells = new ArrayList<WellHead>();
			
			for (WellHead h : f.getWells()) {
				wells.add(h.clone());
			}
			
			for (WellHead h : wells) {
				h.getBores().clear();
				h.getReports().clear();
			}
			
			f.getWells().clear();
			f.getWells().addAll(wells);
			
			fields.add(f);
		}
		
		return fields;
	}
}
