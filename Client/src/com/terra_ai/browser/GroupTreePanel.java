package com.terra_ai.browser;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.business.GroupList;
import com.terra_ai.model.wells.WellHead;

public class GroupTreePanel extends JPanel {
	private GroupList groups;
	private JTree tree;
	
	public GroupTreePanel(GroupList list) {
		super(new BorderLayout());
		groups = list;
		tree = new JTree(new DefaultTreeModel(list));
		add(tree, BorderLayout.CENTER);
	}
	
	public GroupList getGroups() { return groups; }
	
	public TreeNode getSelected() {
		return (TreeNode) tree.getLastSelectedPathComponent();
	}
	
	public Group getSelectedGroup() {
		TreeNode n = getSelected();
		if (n instanceof Group) return (Group) n;
		return null;
	}
	
	public WellHead getSelectedWell() {
		TreeNode n = getSelected();
		if (n instanceof WellHead) return (WellHead) n;
		return null;
	}
	
	public void addSelectionListener(TreeSelectionListener l) {
		tree.addTreeSelectionListener(l);
	}
	
	public void refresh() {
		((DefaultTreeModel) tree.getModel()).reload();
	}
}
