package com.terra_ai.opengl;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.almostrealism.texture.URLImageSource;

import com.almostrealism.explorer.models.Terrain;

public class TerrainPreview extends JPanel {
	public TerrainPreview(Terrain t) {
		
	}
	
	public void show(URLImageSource s) {
		removeAll();
		add(new JLabel(s.getIcon()));
		validate();
	}
}
