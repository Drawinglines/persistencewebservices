function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 4,
		mapTypeId: 'terrain',
		center: new google.maps.LatLng(39.833, -98.583)
	});
	map.enableKeyDragZoom();
	setMap(map);
	return map;
}

function loadFields(updatingState, updatingField, updatingType, updatingStatus) {
	$.getJSON('rest/usa/' + $('#select_state').val() +
				'?field=' + $('#select_field').val() + '&' +
				'wellType=' + $('#select_wellType').val() + '&' +
				'wellStatus=' + $('#select_wellStatus').val() + '&' +
				'token=' + getCookie("token"),
		function (f) {
			setFields(f);
			
			if (getFields().length == 1) {
				loadPage('reports', 'reports.jsp?apis=' + getFields()[0].apis);
			}

			if (!updatingField) {
				$('#select_field').find('option').remove();
				$('#select_field').append('<option></option>')
				
				for (i = 0; i < getFields().length; i++) {
					$('#select_field').append('<option>' + getFields()[i].name + '</option>')
				}
			}
			
			if (!updatingType) loadWellTypes($('#select_wellType'));
			if (!updatingStatus) loadWellStatuses($('#select_wellStatus'));
			loadMarkers();
		}
	);
}

function loadMarkers() {
	clearMarkers();
	
	markers = [];
	
	var infowindow = new google.maps.InfoWindow();
	
	for (var j = 0; j < getFields().length; j++) {
		var f = getFields()[j];
		
		i: for (var i = 0; i < f.wells.length; i++) {
			var marker = new google.maps.Marker({
				position: new google.maps.LatLng(
							f.wells[i].location.coords.latitude,
							f.wells[i].location.coords.longitude),
				icon: 'images/' + f.wells[i].mapDisplayColor + '_circle.png',
	//			map: map
			});
			
			marker.api = f.wells[i].api;
			
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infowindow.setContent(f.wells[i].api.api10 + '<br>' +
											f.wells[i].operator + '<br>' +
											f.wells[i].wellStatus + ' ' +
											f.wells[i].wellType);
					infowindow.open(getMap(), marker);
					loadWell(f.wells[i].api.api10)
				}
			})(marker, i));
			
			markers.push(marker);
		}
	}
	
	setMarkers(markers);
	
	markerClusterer = new MarkerClusterer(getMap(), getMarkers(), { imagePath: 'images/m' });
}

function clearMarkers() {
	if (typeof getMarkers() == 'undefined') return;
	
	for (var i = 0; i < getMarkers().length; i++) {
		getMarkers()[i].setMap(null);
		markerClusterer.removeMarker(getMarkers()[i], i < (getMarkers().length - 1));
	}
}

function selectMarkers(bounds) {
	if (typeof getMarkers() == 'undefined') return;
	
	var apis = "";
	
	for (var i = 0; i < getMarkers().length; i++) {
		if (bounds.contains(getMarkers()[i].getPosition()) == false) {
			getMarkers()[i].setMap(null);
		} else {
			getMarkers()[i].setMap(getMap());
			
			if (apis.length > 0) {
				apis = apis + "," + getMarkers()[i].api.api10
			} else {
				apis = apis + getMarkers()[i].api.api10;
			}
		}
	}
	
	setSelectedAPIs(apis);
	loadPage('reports', 'reports.jsp');
}

function loadWellTypes(formSelect) {
	formSelect.find('option').remove();
	formSelect.append('<option></option>')
	
	types = [];

	for (i = 0; i < getFields().length; i++) {
		for (j = 0; j < getFields()[i].wellTypes.length; j++) {
			if (!types.includes(getFields()[i].wellTypes[j])) types.push(getFields()[i].wellTypes[j]);
		}
	}
	
	types.sort();
	
	for (i = 0; i < types.length; i++) {
		formSelect.append('<option>' + types[i] + '</option>')
	}
}

function loadWellStatuses(formSelect) {
	formSelect.find('option').remove();
	formSelect.append('<option></option>')
	
	statuses = [];
	
	for (i = 0; i < getFields().length; i++) {
		for (j = 0; j < getFields()[i].wellStatuses.length; j++) {
			if (!statuses.includes(getFields()[i].wellStatuses[j])) types.push(getFields()[i].wellStatuses[j]);
		}
	}
	
	statuses.sort();
	
	for (i = 0; i < statuses.length; i++) {
		formSelect.append('<option>' + statuses[i] + '</option>')
	}
}