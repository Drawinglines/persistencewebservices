package com.terra_ai.swing;

import javax.swing.JCheckBox;

import com.terra_ai.model.business.Group;

public class GroupSelector extends JCheckBox {
	private Group group;
	
	public GroupSelector(Group g) { super(g.getName()); group = g; }
	
	public Group getGroup() { return group; }
}
