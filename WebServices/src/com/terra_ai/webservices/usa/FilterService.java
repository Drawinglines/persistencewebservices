package com.terra_ai.webservices.usa;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.almostrealism.html.HTMLFragment;

import io.almostrealism.controls.ComboControl;
import io.almostrealism.controls.ListControl;
import io.almostrealism.controls.NumericRangeControl;
import io.almostrealism.controls.Section;
import io.almostrealism.controls.SectionComponent;
import io.almostrealism.controls.TextControl;

@Path("/filters")
public class FilterService {
	@Path("/list")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getFilters() {
		HTMLFragment f = new HTMLFragment(HTMLFragment.Type.BODY);
		
		/* General */
		
		Section general = new Section("General", true);
		
		SectionComponent api = general.addComponent();
		api.add(new ListControl("API"));
		
		SectionComponent name = general.addComponent();
		name.add(new TextControl("Well Name"));
		
		SectionComponent county = general.addComponent();
		county.add(new ComboControl("County", new ArrayList<String>()));
		
		SectionComponent lease = general.addComponent();
		lease.add(new ComboControl("Lease", new ArrayList<String>()));
		
		SectionComponent field = general.addComponent();
		field.add(new ComboControl("Field", new ArrayList<String>()));
		
		SectionComponent operator = general.addComponent();
		operator.add(new ComboControl("Operator", new ArrayList<String>()));
		
		f.add(general);

		/* Completion */
		
		Section completion = new Section("Completion", true);
		
		SectionComponent type = completion.addComponent();
		type.add(new ComboControl("Well Type", new ArrayList<String>()));
		
		SectionComponent pool = completion.addComponent();
		pool.add(new ComboControl("Pool", new ArrayList<String>()));
		
		SectionComponent depth = completion.addComponent("Depth");
		depth.add(new NumericRangeControl("TVD"));
		depth.add(new NumericRangeControl("MD"));
		depth.add(new NumericRangeControl("Best"));
		
		f.add(completion);
		
		/* Production */
		
		Section production = new Section("Production", true);
		
		SectionComponent status = production.addComponent("Status");
		status.add(new ComboControl("Status Reported", new ArrayList<String>()));
		status.add(new ComboControl("Status Calculated", new ArrayList<String>()));
		status.add(new ComboControl("Last Prod Date", new ArrayList<String>()));
		
		SectionComponent initial = production.addComponent("Initial");
		initial.add(new NumericRangeControl("Oil", "Oil_Initial"));
		initial.add(new NumericRangeControl("Water", "Water_Initial"));
		initial.add(new NumericRangeControl("Gas", "Gas_Initial"));
		initial.add(new NumericRangeControl("WOR", "WOR_Initial"));
		initial.add(new NumericRangeControl("GOR", "GOR_Initial"));
		
		SectionComponent current = production.addComponent("Current");
		current.add(new NumericRangeControl("Oil", "Oil_Current"));
		current.add(new NumericRangeControl("Water", "Water_Current"));
		current.add(new NumericRangeControl("Gas", "Gas_Current"));
		current.add(new NumericRangeControl("WOR", "WOR_Current"));
		current.add(new NumericRangeControl("GOR", "GOR_Current"));
		
		SectionComponent average = production.addComponent("Average");
		average.add(new NumericRangeControl("Oil", "Oil_Average"));
		average.add(new NumericRangeControl("Water", "Water_Average"));
		average.add(new NumericRangeControl("Gas", "Gas_Average"));
		average.add(new NumericRangeControl("WOR", "WOR_Average"));
		average.add(new NumericRangeControl("GOR", "GOR_Average"));
		
		SectionComponent decline = production.addComponent("Decline");
		decline.add(new NumericRangeControl("Oil", "Oil_Decline"));
		decline.add(new NumericRangeControl("Water", "Water_Decline"));
		decline.add(new NumericRangeControl("Gas", "Gas_Decline"));
		decline.add(new NumericRangeControl("WOR", "WOR_Decline"));
		decline.add(new NumericRangeControl("GOR", "GOR_Decline"));
		
		f.add(production);
		
		return f.toHTML();
	}
}
