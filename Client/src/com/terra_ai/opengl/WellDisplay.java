package com.terra_ai.opengl;

import java.io.IOException;

import com.almostrealism.geometry.BasicGeometry;
import com.almostrealism.io.WavefrontObjParser;
import com.almostrealism.replicator.geometry.DefaultReplicant;
import com.almostrealism.util.Vector;
import com.terra_ai.model.business.Group;
import com.terra_ai.model.geo.Point;
import com.terra_ai.model.wells.WellHead;

public class WellDisplay extends DefaultReplicant {
	private Point center;

	public WellDisplay(Group g) throws IOException {
		this(g == null ? null : g.getCenter());
		for (WellHead w : g.getWells()) addWell(w);
	}
	
	public WellDisplay(Point center) throws IOException {
//		super(WavefrontObjParser.parse(Replicator.class.getResourceAsStream("/models/Cube.obj")));
//		super(WavefrontObjParser.parse(WellDisplay.class.getResourceAsStream("/Oil rocker/Oil rocker scaled.obj")));
		super(WavefrontObjParser.parse(WellDisplay.class.getResourceAsStream("/Oil rocker/Oil Rocker 2.obj")));
		this.center = center;
	}
	
	public void addWell(WellHead w) {
		super.put(w.getAPI().toString(), getLocation(w));
	}
	
	public void add(Iterable<WellHead> wells) {
		for (WellHead h : wells) addWell(h);
	}
	
	public void setCenter(Point p) { center = p; }
	
	public Point getCenter() { return center; }
	
	protected BasicGeometry getLocation(WellHead h) {
		Point p = h.getLocation();
		p = p.subtract(center);
		
		BasicGeometry g = new BasicGeometry();
		g.setLocation(new Vector(p.getCoordinates().getLongitude() * 100000, 0.0,
									p.getCoordinates().getLatitude() * 100000));
		return g;
	}
}
