package com.terra_ai.webservices.webgl;

import java.sql.SQLException;
import java.util.Arrays;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.almostrealism.webgl.WebGLExportable;
import com.almostrealism.webgl.WebGLRenderable;
import com.almostrealism.webgl.WebGLExporter;

@Path("/WebGL")
public class BrowserContentService {
	@GET @Produces(MediaType.TEXT_HTML)
	@Path("/render/{group}")
	public String render(@PathParam("group") String group) throws SQLException {
//		Group g = groups.getGroup(group);
		
		WebGLExporter gl = new WebGLExporter(Arrays.asList(new WebGLRenderable() {
			@Override
			public WebGLExportable getWebGLGeometry() {
				return (() -> (() -> "var geometry = new THREE.BoxGeometry( 1, 1, 1 );"));
			}
			
			@Override
			public WebGLExportable getWebGLMaterial() {
				return (() -> (() -> "var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );"));
			}
		}));
		
		return gl.render().toHTML();
	}
}
