package com.terra_ai.webservices.usa;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.terra_ai.filters.GroupGeoFilter;
import com.terra_ai.filters.FilterUtilities;
import com.terra_ai.model.business.Group;
import com.terra_ai.model.forecast.OilProductionForecast;
import com.terra_ai.model.geo.Field;
import com.terra_ai.model.geo.Pool;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.API_2;
import com.terra_ai.model.wells.WellHead;
import ai.terra.persistence.etl.WellHeadByOperatorView;
import ai.terra.persistence.etl.WellHeadByPoolView;
import ai.terra.sql.PublicDataSourceSQL;
import ai.terra.sql.TerraQueryLibrary;

import io.almostrealism.matrix.Tensor;
import io.almostrealism.query.QueryLibrary;
import io.almostrealism.rooms.Rooms;
import io.almostrealism.rooms.Token;

@Path("/usa")
public class FieldService {
	// TODO  This needs to be switched on in production
	public static boolean enableSecurity = false;
	
	private static PublicDataSourceSQL publicData;
	private static Hashtable<String, List<Field>> states;
	private static Hashtable<API_10, WellHead> wells;
	
	static {
		TerraQueryLibrary.initialize();
		publicData = new PublicDataSourceSQL();
		states = new Hashtable<String, List<Field>>();
		wells = new Hashtable<API_10, WellHead>();
	}
	
	@Path("/counties/{state}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<String> getCounties(@PathParam("state") String state,
											@FormParam("query") String q,
											@QueryParam("token") String token)
											throws IllegalAccessException, InvocationTargetException,
												JsonProcessingException, CloneNotSupportedException, IOException {
		Collection<Group> s = getState(state, q, token);
		
		Collection<String> c = new ArrayList<String>();
		
		for (Group g : s) {
			for (WellHead w : g) {
				if (!c.contains(w.getCounty())) c.add(w.getCounty());
			}
		}
		
		return c;
	}
	
	@Path("{state}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Group> getState(@PathParam("state") String state, @FormParam("query") String q, @QueryParam("token") String token)
										throws IllegalAccessException, InvocationTargetException,
											CloneNotSupportedException, JsonProcessingException, IOException {
		FieldQuery query = new ObjectMapper().readValue(q, FieldQuery.class);
		
		if (state == null || state.equalsIgnoreCase("all") || state.length() <= 0) {
			long start = System.currentTimeMillis();
			
			List<Group> groups = new ArrayList<Group>();
			
			for (String s : API_2.list()) {
				groups.addAll(getState(s, q, token));
			}
			
			System.out.println("Retrieved all fields in all states in " + (System.currentTimeMillis() - start) / 60000 + " minutes.");
			
			return groups;
		}
		
		checkToken(token);
		
		List<Field> fields = new ArrayList<Field>();
		fields.addAll(QueryLibrary.root().get(publicData, Field.class, API_2.class, API_2.fromString(state)));
		fields.remove(null);
		Collections.sort(fields);
		cacheFields(state, fields);
		
		Collection<? extends Group> f = FilterUtilities.cloneGroups(states.get(state));
		System.out.println(state + " has " + f.size() + " fields");
		
		f = new FieldQueryOperator(query).applyFilters(f);
		return new GroupGeoFilter().filter(f);
	}
	
	@Path("/well/{api}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public WellHead getWell(@PathParam("api") String api, @QueryParam("token") String token) throws IllegalAccessException,
																								InvocationTargetException,
																								JsonProcessingException,
																								IOException {
		checkToken(token);
		
		WellHead h = wells.get(new API_10(api));
		return h;
	}
	
	@Path("/wells")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Group getWells(@FormParam("apis") String apis, @FormParam("token") String token) throws IllegalAccessException,
																								InvocationTargetException,
																								JsonProcessingException,
																								IOException {
		checkToken(token);
		
		Group g = new Group();
		
		for (String api : apis.split(",")) {
			WellHead h = wells.get(new API_10(api));
			g.addWell(h);
		}
		
		return g;
	}
	
	@Path("/list")
	@POST
	@Produces(MediaType.TEXT_HTML)
	public String listWells(@FormParam("apis") String apis, @FormParam("token") String token) throws IllegalAccessException,
																									InvocationTargetException,
																									JsonProcessingException,
																									IOException {
		checkToken(token);
		
		Tensor<String> matrix = new Tensor<String>();
		
		int index = 0;
		
		for (String api : apis.split(",")) {
			WellHead h = wells.get(new API_10(api));
			matrix.insert(h.getAPI().toString(), index, 0);
			matrix.insert(h.getCounty(), index, 1);
			matrix.insert(h.getOperator(), index, 2);
			matrix.insert(h.getWellStatus(), index, 3);
			matrix.insert(h.getWellType(), index, 4);
			index++;
		}
		
		return matrix.toHTML();
	}
	
	protected static Field getField(API_2 state, String field) throws IllegalAccessException, InvocationTargetException {
		Field f = new Field(state, field);
		QueryLibrary.root().get(publicData, WellHead.class, Field.class, f);
		retrieveForecasts(f);
		processViews(f);
		addWells(f);
		return f;
	}
	
	public static void cacheFields(String state, Collection<Field> fields) {
		long startTime = System.currentTimeMillis();
		
		List<Field> l = states.get(state);
		if (l == null) {
			l = new ArrayList<Field>();
			states.put(state, l);
		}
		
		f: for (Field f : fields) {
			try {
				if (l.contains(f)) continue f;
				
				f = getField(f.getState(), f.getName());
				l.add(f);
				
				System.out.println("FieldService: Added " + f.getName());
			} catch (IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		
		int mins = (int) ((System.currentTimeMillis() - startTime) / 60000);
		
		if (mins > 0) {
			System.out.println("Cached fields for " + state + " in " + mins + " minutes.");
			System.out.println("There are " + wells.size() + " total well heads in all cached fields.");
		}
	}
	
	protected static void retrieveForecasts(Group g) throws IllegalAccessException, InvocationTargetException {
		QueryLibrary.root().get(publicData, OilProductionForecast.class, Group.class, g);
	}
	
	protected static void processViews(Field f) {
		try {
			new WellHeadByOperatorView(publicData, f.getWells()).process();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			Map<Pool, WellHead> pools = new HashMap<Pool, WellHead>();
			
			for (WellHead h : f.getWells()) {
				for (Pool p : h.getPools()) {
					pools.put(p, h);
				}
			}
			
			new WellHeadByPoolView(publicData, pools).process();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * TODO  Expand to deal with permissions.
	 */
	protected static void checkToken(String token) throws IllegalAccessException, JsonProcessingException, InvocationTargetException, IOException {
		System.out.println("Checking token = " + token);
		if (Rooms.getUser(new Token(token)) == null && enableSecurity) throw new IllegalAccessException("Please login");
	}
	
	private static synchronized void addWells(Field f) throws IllegalAccessException, InvocationTargetException {
		for (WellHead h : f.getWells()) {
			wells.put(h.getAPI(), h);
		}
	}
}
