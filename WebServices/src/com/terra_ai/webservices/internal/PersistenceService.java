package com.terra_ai.webservices.internal;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.terra_ai.model.BasicEntity;
import ai.terra.persistence.Sources;

@Path("/Persist")
public class PersistenceService {
//	@Path("/insert/{source}/{map}/{classType}")
//	@POST
	public void insert(String jsonData, @PathParam("source") String source,
					@PathParam("map") String map, @PathParam("classType") String classType) throws Exception {
		ObjectMapper m = new ObjectMapper();
		Sources.valueOf(source).insert(map, (BasicEntity) m.readValue(jsonData, Class.forName(classType)));
	}
	
	@Path("/insert/ffr-metadata")
	@POST
	public void insertFFRMetadata(String jsonData) throws Exception {
		insert(jsonData, "ForecastMetadata", "dynamodb", "com.terra_ai.model.forecast.FFRForecastMetaData");
	}
	
//	@Path("/retrieve")
	public BasicEntity retrieve(String source, String map, long id) throws Exception {
		return Sources.valueOf(source).retrieve(map, id);
	}
}
