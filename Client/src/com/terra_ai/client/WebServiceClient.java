package com.terra_ai.client;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.terra_ai.model.business.Group;
import com.terra_ai.model.business.GroupList;

public class WebServiceClient {
	private ObjectMapper mapper;
	
	private String endpoint;
	
	public WebServiceClient(String endpoint) {
		this.endpoint = endpoint + "/rest/";
		this.mapper = new ObjectMapper();
	}
	
	public GroupList getGroups() throws JsonParseException, JsonMappingException, MalformedURLException, IOException {
		return mapper.readValue(new URL(endpoint + "groups/all"), GroupList.class);
	}
	
	public Group getGroup(String name) throws JsonParseException, JsonMappingException, MalformedURLException, IOException {
		return mapper.readValue(new URL(endpoint + "groups/named/" + name.replaceAll(" ", "%20")), Group.class);
	}
}
