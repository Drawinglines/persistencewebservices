package com.terra_ai.browser;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.DefaultListModel;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import ai.terra.google.MapsImageProvider;
import com.terra_ai.model.business.Group;
import com.terra_ai.model.business.GroupList;
import com.terra_ai.model.wells.WellHead;
import com.terra_ai.opengl.TerrainPreview;
import com.terra_ai.opengl.WellDisplay;
import com.terra_ai.swing.CheckBoxList;
import com.terra_ai.swing.GroupSelector;

/**
 * TODO  The static reporting methods here should be moved into reports classes.
 * 
 * @author  Michael Murray
 */
public class Browser extends JPanel {
	private BrowsablePlanet planet = new BrowsablePlanet();
	
	private TerrainCanvas canvas;
	private GroupTreePanel groupsTree;
	private WellDisplay wells;
	
	private JPanel rightPanel = new JPanel(new BorderLayout());
	
	private JEditorPane reportsPane = new JEditorPane();
	private TerrainPreview terrainPreview = new TerrainPreview(null);
	
	public Browser(GroupList list) throws IOException {
		super(new BorderLayout());
		
		this.canvas = new TerrainCanvas();
		this.groupsTree = new GroupTreePanel(list);
		
		groupsTree.addSelectionListener((e) -> { refreshCanvas(); });
		
		ValueSlider zoom = new ValueSlider(ValueSlider.HORIZONTAL, 0, 2000, 600);
		this.canvas.setZoom(zoom);
		
		initCanvas();
		
		JSplitPane pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, new JScrollPane(groupsTree), canvas);
		pane.setDividerLocation(0.3);
		
		JMenuItem exportObj = new JMenuItem("Export OBJ");
		
		JMenuItem addGroupItem = new JMenuItem("Add group");
		addGroupItem.addActionListener((e) -> { doAddGroups(); });
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(exportObj);
		fileMenu.add(addGroupItem);
		
		JMenu tableauItems = new JMenu("Tableau");
		
		JMenuItem fieldItem = new JMenuItem("Field");
		fieldItem.addActionListener((e) -> { openTableau(groupsTree.getSelectedGroup().getName()); });
		
		JMenuItem overview = new JMenuItem("Overview");
		overview.addActionListener((e) -> { browse("http://52.11.110.240/#/views/OverviewDB_0/OverviewDB1"); });

		tableauItems.add(overview);
		tableauItems.add(fieldItem);
		
		JMenuItem reportsItem = new JMenuItem("Reports");
		reportsItem.addActionListener((e) -> { openReports(String.valueOf(groupsTree.getSelectedWell().getAPI().getDigits())); });
		
		JMenu analyticsMenu = new JMenu("Analytics");
		analyticsMenu.add(tableauItems);
		analyticsMenu.add(reportsItem);
		
		JMenuBar bar = new JMenuBar();
		bar.add(fileMenu);
		bar.add(analyticsMenu);
		
//		rightPanel.add(reportsPane, BorderLayout.CENTER);
		rightPanel.add(terrainPreview, BorderLayout.SOUTH);
		
		add(pane, BorderLayout.CENTER);
		add(bar, BorderLayout.NORTH);
		add(rightPanel, BorderLayout.EAST);
		add(zoom, BorderLayout.SOUTH);
	}
	
	public void start() { canvas.start(); }
	
	protected void initCanvas() throws IOException {
		refreshCanvas();
	}
	
	protected void refreshCanvas() {
		Group g = groupsTree.getSelectedGroup();
		
		if (wells == null && g != null) {
			try {
				wells = new WellDisplay(g);
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		try {
			if (g != null) {
				canvas.removeAll();
				
				wells.clear();
				wells.setCenter(g.getCenter());
				wells.add(g);
				
				canvas.add((Renderable) wells);
				initTerrain();
				canvas.reset();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		WellHead h = groupsTree.getSelectedWell();
		if (h != null) {
			try {
				reportsPane.setPage("http://terra.us-west-2.elasticbeanstalk.com/reports.jsp?api=" +
									h.getAPI().getDigits());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (wells != null) {
				canvas.lookAt(h.getLocation().transform(wells.getCenter()));
				canvas.reset();
			}
		}
	}
	
	protected void initTerrain() throws MalformedURLException {
		Terrain t = new Terrain(10000, 10000);
		t.getTexture().clear();
		t.getTexture().addLayers(maps.getImage(wells.getCenter()));
		canvas.add((Renderable) t);
		terrainPreview.show((URLImageSource) t.getTexture().getLayer("satellite"));
	}
	
	public static void openTableau(String fieldName) {
		if (fieldName == null) return;
		browse("http://52.11.110.240/views/Multipledashboards/Dashboard3?:linktarget=_right&:embed=y&Field%20Name=" + fieldName.replaceAll(" ", "%20"));
	}
	
	public void openReports(String wellApi) {
		browse("http://pws.terra.ai/reports.jsp?api=" + wellApi);
	}
	
	public static void browse(String uri) {
		try {
			Desktop.getDesktop().browse(new URI(uri));
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	public void doAddGroups() {
		CheckBoxList l = new CheckBoxList();
		
		GroupList allGroups;
		try {
			allGroups = client.getGroups();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		for (Group g : allGroups) {
			l.addCheckbox(new GroupSelector(g));
		}

		GroupList selectedGroups = new GroupList();
		
		JOptionPane.showMessageDialog(null, new JScrollPane(l), "Select Groups", JOptionPane.PLAIN_MESSAGE);
		
		for (int i = 0; i < ((DefaultListModel) l.getModel()).getSize(); i++) {
			GroupSelector s = (GroupSelector) ((DefaultListModel) l.getModel()).get(i);
			if (s.isSelected()) {
				try {
					selectedGroups.add(client.getGroup(s.getGroup().getName()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		this.groupsTree.getGroups().addAll(selectedGroups);
		this.groupsTree.refresh();
	}
	
	public static void main(String args[]) throws JsonParseException, JsonMappingException, MalformedURLException, IOException {
		Browser b = new Browser(new GroupList());
		b.start();
		
		JFrame f = new JFrame("Terra AI Browser");
		f.getContentPane().add(b);
		
		f.setSize(1000, 800);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
}
