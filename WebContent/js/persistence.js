/** Insert an entity using the PersistenceService. */
function insertEntity(form, url) {
	var fields = {};
	$(form).find("input, textarea, select").each(function() {
		var inputType = this.tagName.toUpperCase() === "INPUT" && this.type.toUpperCase();
		if (inputType !== "BUTTON" && inputType !== "SUBMIT") {
			s = $(this).val();
			
			if (s.startsWith("[")) {
				fields[this.name] = JSON.parse(s);
			} else {
				fields[this.name] = s;
			}
		}
	});
	
	$.ajax({
		type: "POST",
		url: url,
		dataType: "json",
		traditional: true,
		
		success: function () {
			alert("Data was added!");
			location.reload();
		},
		
		data: JSON.stringify(fields)
	});
}