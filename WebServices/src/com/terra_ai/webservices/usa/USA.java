package com.terra_ai.webservices.usa;

import java.sql.SQLException;
import java.util.Collection;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.lucene.document.Document;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.terra_ai.model.wells.API_10;
import com.terra_ai.model.wells.WellBore;
import ai.terra.query.SearchEngine;
import ai.terra.sql.PublicDataSourceSQL;
import ai.terra.tableau.DataConnectorUtils;

import io.almostrealism.persist.AlmostCache;

/**
 * The {@link RegionService} for the USA.
 * 
 * TODO Move SearchEngine feature of this class over to FieldService (or another service).
 * 
 * @author  Michael Murray
 */
//@Path("/USA")
@Deprecated
public class USA extends SearchEngine<WellBore> {
	public AlmostCache<PublicDataSourceSQL, API_10> wells;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	public USA() throws SQLException {
		wells = new AlmostCache<PublicDataSourceSQL, API_10>(new PublicDataSourceSQL());
	}
	
	public Collection<WellBore> getData() throws SQLException { throw new SQLException(); }
	
	@Override
	public WellBore getSearchableForDocument(Document d) throws SQLException {
		return getWell(d.get("api"));
	}
	
	@GET @Produces(MediaType.APPLICATION_JSON)
	@Path("/wells")
	public String wells(@QueryParam("q") String query) throws Exception {
		reindex(); // TODO  We should be reindexing on a schedule, rather than every time (very inefficient)
		return DataConnectorUtils.createPage(mapper.writeValueAsString(query(query)));
	}
	
	@GET @Produces(MediaType.APPLICATION_JSON)
	@Path("/well/{api}")
	public WellBore getWell(@PathParam("api") String api) throws SQLException {
		return wells.get(WellBore.class, new API_10(api));
	}
	
	@GET @Produces(MediaType.APPLICATION_JSON)
	@Path("/random/{state}")
	public String randomWell(@PathParam("state") String state) throws JsonProcessingException, SQLException {
//		return DataConnectorUtils.createPage(mapper.writeValueAsString(db.getWell(db.randomWell().getAPI10()).getWellHead()));
		throw new SQLException();
	}
}
