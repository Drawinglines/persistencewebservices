package com.terra_ai.filters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.terra_ai.model.business.Group;
import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.ValueFilter;

public class FilterUtilities {
	private FilterUtilities() { }
	
	public static <V extends Group> Collection<V> cloneGroups(Collection<V> c) {
		Collection<V> groups = new ArrayList<V>();
		
		for (Group g : c) {
			try {
				groups.add((V) g.clone());
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		
		return groups;
	}
	
	public static <V extends Group> void filterGroups(Collection<V> c, ValueFilter<WellHead> h) {
		for (Group g : c) {
			try {
				g.setWells((List<WellHead>) h.filter(g.getWells()));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static String preprocessFilterValue(String value) {
		if (value != null && value.equals("null")) return null;
		return value;
	}
}
