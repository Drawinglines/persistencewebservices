

function getWell(w) {
	var well = [[2]];
	
	$.ajax({
		  url: 'http://10.37.129.2:8080/PersistenceWebServices/rest/USA/well/' + w,
		  async: false,
		  success: function (data) {
			  well = [[data.reports]];
		  },
		  error: function(xhr, textStatus, error) {
			  var result = "Error: " + error + " StatusCode: " + textStatus + " XHR: " + xhr.readyState;
			  well = [[result]];
		  }  
		});
    
	console.info(well);
	
    return well;
}