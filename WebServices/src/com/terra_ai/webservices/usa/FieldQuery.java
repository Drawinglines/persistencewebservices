package com.terra_ai.webservices.usa;

public class FieldQuery {
	private String apis;
	private String wellName;
	private String county;
	private String lease;
	private String field;
	private String operator;
	private String wellType;
	private String pool;
	private String tvd[];
	private String md[];
	private String statusReported;
	private String statusCalculated;
	private String lastProdDate;
	private String initialOil[];
	private String initialWater[];
	private String initialGas[];
	private String initialWor[];
	private String initialGor[];
	private String currentOil[];
	private String currentWater[];
	private String currentGas[];
	private String currentWor[];
	private String currentGor[];
	private String averageOil[];
	private String averageWater[];
	private String averageGas[];
	private String averageWor[];
	private String averageGor[];
	private String declineOil[];
	private String declineWater[];
	private String declineGas[];
	private String declineWor[];
	private String declineGor[];

	public String getApis() { return apis; }
	public void setApis(String apis) { this.apis = apis; }
	public String getWellName() { return wellName; }
	public void setWellName(String wellName) { this.wellName = wellName; }
	public String getCounty() { return county; }
	public void setCounty(String county) { this.county = county; }
	public String getLease() { return lease; }
	public void setLease(String lease) { this.lease = lease; }
	public String getField() { return field; }
	public void setField(String field) { this.field = field; }
	public String getOperator() { return operator; }
	public void setOperator(String operator) { this.operator = operator; }
	public String getWellType() { return wellType; }
	public void setWellType(String wellType) { this.wellType = wellType; }
	public String getPool() { return pool; }
	public void setPool(String pool) { this.pool = pool; }
	public String[] getTvd() { return tvd; }
	public void setTvd(String tvd[]) { this.tvd = tvd; }
	public String[] getMd() { return md; }
	public void setMd(String md[]) { this.md = md; }
	public String getStatusReported() { return statusReported; }
	public void setStatusReported(String statusReported) { this.statusReported = statusReported; }
	public String getStatusCalculated() { return statusCalculated; }
	public void setStatusCalculated(String statusCalculated) { this.statusCalculated = statusCalculated; }
	public String getLastProdDate() { return lastProdDate; }
	public void setLastProdDate(String lastProdDate) { this.lastProdDate = lastProdDate; }
	public String[] getInitialOil() { return initialOil; }
	public void setInitialOil(String initialOil[]) { this.initialOil = initialOil; }
	public String[] getInitialWater() { return initialWater; }
	public void setInitialWater(String initialWater[]) { this.initialWater = initialWater; }
	public String[] getInitialGas() { return initialGas; }
	public void setInitialGas(String initialGas[]) { this.initialGas = initialGas; }
	public String[] getInitialWor() { return initialWor; }
	public void setInitialWor(String initialWor[]) { this.initialWor = initialWor; }
	public String[] getInitialGor() { return initialGor; }
	public void setInitialGor(String initialGor[]) { this.initialGor = initialGor; }
	public String[] getCurrentOil() { return currentOil; }
	public void setCurrentOil(String currentOil[]) { this.currentOil = currentOil; }
	public String[] getCurrentWater() { return currentWater; }
	public void setCurrentWater(String currentWater[]) { this.currentWater = currentWater; }
	public String[] getCurrentGas() { return currentGas; }
	public void setCurrentGas(String currentGas[]) { this.currentGas = currentGas; }
	public String[] getCurrentWor() { return currentWor; }
	public void setCurrentWor(String currentWor[]) { this.currentWor = currentWor; }
	public String[] getCurrentGor() { return currentGor; }
	public void setCurrentGor(String currentGor[]) { this.currentGor = currentGor; }
	public String[] getAverageOil() { return averageOil; }
	public void setAverageOil(String averageOil[]) { this.averageOil = averageOil; }
	public String[] getAverageWater() { return averageWater; }
	public void setAverageWater(String averageWater[]) { this.averageWater = averageWater; }
	public String[] getAverageGas() { return averageGas; }
	public void setAverageGas(String averageGas[]) { this.averageGas = averageGas; }
	public String[] getAverageWor() { return averageWor; }
	public void setAverageWor(String averageWor[]) { this.averageWor = averageWor; }
	public String[] getAverageGor() { return averageGor; }
	public void setAverageGor(String averageGor[]) { this.averageGor = averageGor; }
	public String[] getDeclineOil() { return declineOil; }
	public void setDeclineOil(String declineOil[]) { this.declineOil = declineOil; }
	public String[] getDeclineWater() { return declineWater; }
	public void setDeclineWater(String declineWater[]) { this.declineWater = declineWater; }
	public String[] getDeclineGas() { return declineGas; }
	public void setDeclineGas(String declineGas[]) { this.declineGas = declineGas; }
	public String[] getDeclineWor() { return declineWor; }
	public void setDeclineWor(String declineWor[]) { this.declineWor = declineWor; }
	public String[] getDeclineGor() { return declineGor; }
	public void setDeclineGor(String declineGor[]) { this.declineGor = declineGor; }
}
