package com.terra_ai.test;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JSlider;

public class TestWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TestWindow window = new TestWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TestWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTextPane textPane = new JTextPane();
		frame.getContentPane().add(textPane, BorderLayout.CENTER);
		
		JToolBar toolBar = new JToolBar();
		frame.getContentPane().add(toolBar, BorderLayout.NORTH);
		
		JButton btnTest = new JButton("Test");
		toolBar.add(btnTest);
		
		JButton btnTgafahd = new JButton("Tgafahd");
		toolBar.add(btnTgafahd);
		
		JEditorPane dtrpnTedagljdfga = new JEditorPane();
		dtrpnTedagljdfga.setText("Tedagljdfga");
		frame.getContentPane().add(dtrpnTedagljdfga, BorderLayout.WEST);
		
		JSlider slider = new JSlider();
		frame.getContentPane().add(slider, BorderLayout.SOUTH);
		
		JSlider slider_1 = new JSlider();
		frame.getContentPane().add(slider_1, BorderLayout.EAST);
	}

}
