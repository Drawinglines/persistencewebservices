function getSearchParameters() {
	var prmstr = window.location.search.substr(1);
	return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
}

function transformToAssocArray(prmstr) {
	var params = {};
	var prmarr = prmstr.split("&");
	for ( var i = 0; i < prmarr.length; i++) {
		var tmparr = prmarr[i].split("=");
		params[tmparr[0]] = tmparr[1];
	}
	return params;
}

function loadPage(id, src) {
	top.frames[id].src = src;
}

function checkLogin() {
	$.post("rest/login/check/", { token : getCookie("token") },
		function(valid) {
			if (valid == "false")
				window.top.location.href="login.html";
		}
	);
}

function loadWell(api) {
	setSelectedAPIs(api);
	loadPage('reports', 'reports.jsp');
	loadPage('details', 'details.html?api=' + api);
}

function setField(f) { window.top.field = f; }

function getField() { return window.top.field; }

function setFields(f) { window.top.fields = f; }

function getFields() { return window.top.fields; }

function setSelectedAPIs(apis) { window.top.selected_apis=apis; }

function getSelectedAPIs() { return window.top.selected_apis; }

function setMap(m) {
	console.info("Setting map to " + m);
	window.top.map = m;
}

function getMap() {
	console.info("Retrieving map " + window.top.map);
	return window.top.map;
}

function setMarkers(markers) { window.top.markers = markers; }
function getMarkers() { return window.top.markers; }

function setCookie(cname, cvalue) {
	var d = new Date();
    d.setTime(d.getTime() + (1*24*60*60*1000)); // One day
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}