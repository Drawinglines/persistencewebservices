package com.terra_ai.webservices.internal;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.terra_ai.model.Document;
import ai.terra.s3.Bucket;

@Path("/Images")
public class ImageService {
	static {
		Iterator<ImageReader> itr = ImageIO.getImageReadersByFormatName("tiff");
		
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
	
	@GET
	@Path("/ocr")
	@Produces("image/png")
	public Response getOCRImage() throws AmazonServiceException, AmazonClientException, IOException {
		Bucket b = new Bucket("mechanicalturk-dirsurvey-ocr-images");
		
		Document d = b.find("DirectionalSurvey").iterator().next();
		
		BufferedImage image = d.getImage();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);
		byte[] imageData = baos.toByteArray();
		return Response.ok(new ByteArrayInputStream(imageData)).build();
	}
}
