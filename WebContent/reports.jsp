<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<script type="text/javascript" src="js/jquery-2.2.1.js"></script>
	<script type="text/javascript" src="bootstrap-3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="bootstrap-3.3.7/css/bootstrap.css">
	
	<link rel="stylesheet" type="text/css" href="css/reports.css" />
	<link rel="stylesheet" type="text/css" href="css/common.css">
	
	<script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
	<script type="text/javascript" src="js/d3.geo.js"></script>
	<script type="text/javascript" src="js/d3.geom.js"></script>
	<script type="text/javascript" src="js/jquery-2.2.1.js"></script>
	<script type="text/javascript" src="http://d3js.org/topojson.v1.min.js"></script>
	<script type="text/javascript" src="js/util.js"></script>
	<script type="text/javascript" src="js/reports.js"></script>
	
	<title>Well Report</title>
</head>
<body>
	
	<div class="container-fluid">
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#production">Production History</a></li>
		<li><a data-toggle="tab" href="#">Wedges</a></li>
		<li><a data-toggle="tab" href="#">Failure</a></li>
		<li><a data-toggle="tab" href="#">Activity</a></li>
		<li><a data-toggle="tab" href="#grouping">Grouping</a></li>
		<li><a data-toggle="tab" href="#well_list">List</a></li>
	</ul>
	
	<div class="tab-content">
	
		<div id="production" class="tab-pane fade in active">
			<div class="row">
				<div id='Linechart'></div>
			</div>
			<div class="row" style="position: absolute; top:650px; left:600px; width: 100%">
			<!--
				<form class="form-horizontal">
					  <div class="col-md-3">
					    <button id="customizeChartsButton" name="customizeChartsButton" class="btn btn-primary">Customize Charts</button>
					  </div>
					  <div class="col-md-3">
					    <button id="newWindowButton" name="newWindowButton" class="btn btn-primary">New Window</button>
					  </div>
					  <div class="col-md-3">
					    <button id="backtestButton" name="backtestButton" class="btn btn-primary">Backtest</button>
					  </div>
					  <div class="col-md-3">
					    <button id="forecastButton" name="forecastButton" class="btn btn-success">Forecast</button>
					  </div>
				</form>
				 -->
			</div>
		</div>
		
		<div id="grouping" class="tab-pane in fade">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#operators">Operators</a></li>
				<li><a data-toggle="tab" href="#pools">Pools</a></li>
			</ul>
			
			<div class="tab-content">
				<div id="operators" class="tab-pane fade in active">
					<iframe
					  width="800"
					  height="600"
					  seamless
					  frameBorder="0"
					  scrolling="no"
					  id="operators_iframe">
					</iframe>
				</div>
				
				<div id="pools" class="tab-pane fade">
					<iframe
					  width="800"
					  height="600"
					  seamless
					  frameBorder="0"
					  scrolling="no"
					  id="pools_iframe">
					</iframe>
				</div>
			</div>
		</div>
		
		<div id="well_list" class="tab-pane fade">
		</div>
	</div>
	</div>
	
	<script>
	linechart(getSelectedAPIs());
	populateOperators(getSelectedAPIs());
	populatePools(getSelectedAPIs());
	populateWellList(getSelectedAPIs());
	</script>
</body>
</html>