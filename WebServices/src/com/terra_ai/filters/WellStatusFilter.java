package com.terra_ai.filters;

import java.util.ArrayList;
import java.util.Collection;

import com.terra_ai.model.wells.WellHead;

import io.almostrealism.persist.ValueFilter;

public class WellStatusFilter extends ValueFilter<WellHead> {
	private String status;
	
	public WellStatusFilter(String wellStatus) { this.status = wellStatus; }
	
	@Override
	public Collection<WellHead> filter(Collection<? extends WellHead> values) throws CloneNotSupportedException {
		Collection<WellHead> wells = new ArrayList<WellHead>();
		for (WellHead h : values) if (status.equals(h.getWellStatus())) wells.add(h);
		return wells;
	}
}
